clear;clc; close all;

% Settings
TR = 0.72;                      % sec
freq_samp = 30;            % interpolation factor for TR sampling
In_TR = TR/freq_samp;  % Sampling time after interpolation
delay_thresh = 5;           % Delay threshold (sec)

% Load time series
ts_V1 = importdata('ts_V1.txt');
ts_V2 = importdata('ts_V2.txt');
ts_V3d = importdata('ts_V3d.txt');
ts_V3A = importdata('ts_V3A.txt');

ts = [ts_V1, ts_V2, ts_V3d, ts_V3A];
Nts = size(ts, 1);      % number of time points
Nvox = size(ts, 2);    % number of voxels

% Normalize the time series (demean and standardize)
ts_norm = ts - repmat(mean(ts),size(ts,1),1);
ts_norm = ts_norm/std(ts_norm(:));

% Compute time lag
TauMat = zeros(Nvox, Nvox);
fprintf('     Processing percentage: ');
temp_perc = 1;
for i = 1:Nvox
    perc = (i / Nvox) * 100;
    if perc >= temp_perc * 5
        fprintf('%.0f.. ',perc);
        temp_perc = temp_perc + 1;
    end
    
    for j = i+1:Nvox
        % compute cross covariance
        [cc, lags] = xcov(ts_norm(:,i), ts_norm(:,j));    % cross-covariance with lags
        
        % interpolation
        In_cc = interp(cc, freq_samp);  % interpolate cross-covariance
        In_lags = -((Nts-1)*freq_samp) : (((Nts-1)*freq_samp)+(freq_samp-1)); % Interpolate lags
        
        % compute tau
        abs_argmax_idx = find(abs(In_cc) == max(abs(In_cc)));   % find extremum (consider both positive and negative values)
        tau = In_lags(abs_argmax_idx) * In_TR;
        TauMat(i,j) = tau;
    end
end
fprintf('\n');
TauMat = TauMat - TauMat';

% Threshold the TauMat with delay threshold
idx = find(abs(TauMat(:)) > delay_thresh);
TauMatThr = TauMat;
TauMatThr(idx) = NaN;

% Plot the time delay matrix
figure; imagesc(TauMatThr); caxis([-5 5]); colorbar;
set(gca,'FontSize',12);
title('Time delay matrix', 'fontsize', 15, 'fontweight', 'bold');
