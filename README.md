# LagStructure

Lag structure for fMRI time series: Infer directionality

* **Please cite the paper if you use this code** \
*B.-y. Park, W. M. Shim, O. James, and H. Park.* Possible links between the lag structure in visual cortex and visual streams using fMRI. *Scientific Reports* 9:4283 (2019). \
https://www.nature.com/articles/s41598-019-40728-x

# Prerequisite

* **MATLAB >= v.2016b** 

# Websites

* **LAB (CAMIN: Computational Analysis for Multimodal Integrative Neuroimaging):** https://www.caminlab.com/
* **LAB (MIPL: Medical Image Processing Lab):** https://mipskku.wixsite.com/mipl